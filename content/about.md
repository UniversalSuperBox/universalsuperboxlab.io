---
title: About
---

I went down too many rabbit holes and now I have the cursed knowledge.

I'm a co-host of [Linux After Dark](https://linuxafterdark.net). Sometimes I write blog posts about things I do. Most of the time they're niche, arcane, frivolous, or a combination thereof. You might like that, though.

## Contact

You can contact me by email at blog@maydur.st

I use a variant of UniversalSuperBox as my display name in most places, but you definitely have an email address.
