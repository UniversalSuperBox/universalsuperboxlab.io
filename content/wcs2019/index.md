---
title: "2019 Wisconsin Canvas Summit"
date: 2019-05-19
---

This post is a companion to my short add-on talk at the [2019 Wisconsin Canvas Summit](https://sites.google.com/cvtc.edu/wisconsin-canvas-summit-2019/home). Here you will find links to resources to create your own Canvas API/LTI projects using Python.

## Slides

The slides for the presentation are available in two formats:

* [HTML](slides.html) (Opens directly in web browser)

## Projects you should check out

The University of Central Florida has released many Open Source projects that will help you get started.

### CanvasAPI

CanvasAPI gives you access to data from Canvas in an easy-to-use format. According to the project, it "enables developers to programmatically manage Canvas courses, users, gradebooks, and more."

You can find CanvasAPI, including links to its extensive documentation, on [GitHub](https://github.com/ucfopen/canvasapi).

### LTI Template: Flask with OAUTH tokens

The Flask with OAUTH Tokens template helps you create an LTI that acts as its user when accessing Canvas. This ensures that the LTI can find the data it needs to create submissions or view grades, for example.

You can find the template on [GitHub](https://github.com/ucfopen/lti-template-flask-oauth-tokens). There are more templates available in [the UCF GitHub organization](https://github.com/ucfopen), search for `lti`.

## CVTC's Early Alert system

Canvas Course Engagement complements our Early Alert system both before and after the alert is sent. Ellucian has published a case study, [*Helping students before they even know they need help*](https://www.ellucian.com/assets/en/success-story/success-story-chippewa-valley-technical-college-helping-students-they-know-they-need-help.pdf), which gives an overview of the Early Alert system and its implementation using Ellucian CRM Advise.

## Research for Canvas Course Engagement

The Canvas Course Engagement project is based on research conducted at the University of Michigan. It replicates their algorithm in a simpler codebase with fewer features than their [Student Explorer](https://ai.umich.edu/portfolio/student-explorer/) suite.

### Student Explorer codebase

You can find the code for Student Explorer on [GitHub](https://github.com/tl-its-umich-edu/student_explorer).

### InstructureCon 2015 Talk

Steve Lonn gave a talk on Student Explorer and its related research at InstructureCon 2015: Using Canvas APIs to Serve a Campus Early Warning System. The [Canvas Community post](https://community.canvaslms.com/events/1193) includes links to a video of the talk and papers generated from the research.
