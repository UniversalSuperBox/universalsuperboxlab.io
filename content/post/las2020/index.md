---
title: "Brickless Battery Saving with Ubuntu Touch at Linux App Summit 2020"
date: 2020-11-14
---

This post is a companion to my talk at [Linux App Summit 2020](https://linuxappsummit.org/). This talk was all about giving people the smartphone experience while also saving battery on our Linux-powered mobile devices.

## Watch the talk

*Updated January 4, 2020*: The talk has been posted on YouTube for quite some time, but I never posted it here... Whoops! You can check out the whole talk at ["Brickless battery saving with Ubuntu Touch - Dalton Durst" on the LAS YouTube channel](https://www.youtube.com/watch?v=tt1U677h5kY).

## Get in touch

If you picked up what I was putting down during this talk, please contact me. I'm going to turn the concepts I discussed into reality, having more input on how the reality should differ from the concepts is helpful.

* brickless at daltondur.st
* Telegram: UniversalSuperBox
* Matrix: @UniversalSuperBox:matrix.org
* Twitter: [@UnivrslSuprBox](https://twitter.com/UnivrsalSuprBox/)

## Slides

The slides for the presentation are available in two formats:

* [PDF](Brickless.pdf)
* [ODP](Brickless.odp)
