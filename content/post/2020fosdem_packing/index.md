---
title: "The stuff I took to FOSDEM 2020"
date: 2020-02-10T08:21:31-06:00
---

Recently I went to [FOSDEM](https://fosdem.org/2020/) in Brussels, followed by a week-long sprint in Berlin with the KDE Plasma Mobile team. This nine-night trip would require five flights in total, one on an ultra-cheap European airline.

I may write another post about my experiences at FOSDEM and the sprint, but this isn't the one. This post lists everything I took on the trip, even the small trivial items, for my own memory. I've also included some interesting points about the things I took and how I'd change my carried items in the future.

An important goal of this trip, to me, was taking no more than carry-on luggage. I like the feeling of not having too many things at a time. Also, checking baggage is expensive. Also, I'm chronically bad at getting to the airport with enough time to check baggage.

Being a technologist going to a technology conference, I needed to take many things to develop or showcase. This reduced my amount of personally usable space and weight. For the task I picked my trusty Timbuk2 Flight (Small) messenger bag and bought a Jansport Right Pack (Digital Edition) backpack. Together, these bags gave me about 43 liters of usable space.

On the recommendation of OneBag enthusiasts everywhere, I picked up [a cheap 5-piece set of packing cubes from Amazon](https://smile.amazon.com/gp/product/B00HHBR00I). I could only use the three smaller cubes in the set, the larger two were too large to load in either bag.

With the space and length of time, this trip required washing clothes. This wasn't a problem in either Brussels or Berlin, there were plenty of options for laundromats... we just had to find one that *didn't* contain someone clearly on hard drugs. This ended up being a laundromat across the street from a Police station in Brussels, followed by an internet-connected option in Berlin that might be the muse for a ranty blog post someday.

I'm not sponsored by any of the brands on this list. I include a suspicious amount of detail so no one needs to ask for more.

## Backpack

I placed lighter, bulkier items that would not need to be moved in airport security in my Jansport Right Pack (Digital Edition) backpack.

* Winter (read: heavy) jacket
* Rain jacket
* Large packing cube, containing:
  * 2x moisture-wicking undershirts
  * 3x light t-shirts
  * Polo shirt
* Large packing cube, containing:
  * Outdoor pants
  * Jeans
  * Freshening up kit, containing:
    * Toothbrush
    * Razor (and 2 cartridges)
    * 10 packets Emergen-C
    * Tiny, travel-size antiperspirant
* 5x pairs underwear
* 5x pairs socks
* Sunglasses

Quick bag review: The Right Pack is a fine backpack, especially with light items. It's also got a lifetime warranty. A poorly-done bulky load prevents the pack from balancing well. With a better-packed load, the Right Pack is comfortable. I should have gone with the non-Digital Edition model. It would have a bit more internal storage not taken up by the cushioning for the laptop and tablet area. I considered using the backpack as a daypack to justify these features, but my messenger bag was already packed and I prefer to wear it anyway.

I wish I had left the winter jacket in my car at the airport. The weather was relatively warm in Belgium and Germany, my light jacket and rain jacket (serving as a windbreak most days) were already too warm. The winter jacket was the main cause of my space woes. Maybe a more compact one is in order.

The "outdoor pants" listed are Wrangler "Outdoor" something-or-others in black. They're amazingly light, comfortable, and quick-drying. I'll be picking up more pairs of these for domestic and international use. Jeans take too long to dry.

The jury's still out on whether Emergen-C effectively prevents infectious diseases. However, I found that it was rather cheap and wanted to try it. I ended up not getting sick. That's an impressive feat given I was at a conference attended by thousands, talking to many people and being in close contact with several who were sick. However, it's more likely that using hand sanitizer and washing my hands often were much better at preventing disease than the supplement.

I accidentally left the polo shirt at my first hotel in Brussels. Not a big loss, and it proves that I didn't need it.

## Messenger bag

My Timbuk2 Flight (Small) messenger bag, being easier to pack and unpack quickly, stored all of my electronics and served as my day pack. Additionally, I found that a smaller packing cube was able to fit all of my electronics that were larger than cell phones. This made it easier to pass through security in fewer steps:

1. Place backpack in bin
1. Place jacket, belt, and shoes in bin
1. Remove liquids from messenger bag
1. Remove laptops from messenger bag
1. Remove packing cube from messenger bag
1. Spread out items from packing cube in a bin

The bag's contents were small but heavy:

* Dell XPS 13 9370
* Pine64 PineBook Pro
* Targus USB-C Ethernet, HDMI, VGA, USB-A dongle
* RocketBook Everlast Legal
* 2x Microfiber cloth (one is better for erasing the RocketBook, the other for cleaning screens)
* Frixion 4 pen
* Frixion black pen
* 2x boring pens
* Sony earbuds
* Small packing cube
  * Kobo Aura H2O Edition 2
  * Monoprice USB-C and USB-A charger
  * CAD U37 Microphone, tripod, and USB Mini-B cable
  * 2x cheap 2000 mAh power banks
  * LG Nexus 5
  * 2x Pine64 PinePhone
* Microsoft Display Adapter
* A few sticks of gum and a pack of Icebreakers in a 1l zippable plastic bag
* Bottle of Cetrizine tablets
* 3x portable tissue packs
* Bag of mixed nuts
* Portable non-fragile mirror
* Small parts box, containing:
  * USB-eMMC adapter for Pine64 products
  * 32GB Flash drive
  * 2x USB C-A dongle
  * USB C power meter
  * USB A, micro-B, or C SD card reader
* US-EU plug adapter
* 6-foot USB-C cable
* 3-foot USB-C cable
* USB C-Micro B cable
* USB A-Micro B cable
* USB A-C cable
* SlimPort adapter
* USB 3.0 4-port hub
* Lip balm
* Lanyard containing second-factor authentication devices
* Nail clippers
* Liquids bag, containing:
  * Trial-size toothpaste
  * Trial-size Shave Butter
  * Trial-size Post-Shave Dew
  * Hair styling product
  * 2x travel hand sanitizer containers

I really like the Timbuk2 bag. It's small enough to fit under an airline seat lengthwise, leaving a bit of space to its left or right. The space it leaves is enough for one leg. That doesn't sound like much, but the little bit more legroom is amazing on 9-hour flights.

The bag of mixed nuts made it very difficult to get everything packed for each trip through security before it was depleted (in total 2). It was one of those last-minute items that I tend to regret. I will take a much smaller bag next time, or resolve myself to overpaying for them in the clean area of the airport. It's not *that* much money, after all.

I really love having the XPS 13, a super small but powerful laptop. If you need to do international travel but need a stupid amount of performance on the go, I would not hesitate to pick one up. Of course it's useful for more than travel (it's my main workstation, docked on my desk at all times), but it's *especially* nice for travel. Its low weight was offset by the additional laptop, the Pine64 PineBook Pro, in my bag. Both laptops are light on their own, but heavy together. However, duty called. I needed the PineBook for FOSDEM.

All of these devices charge using USB, which means I only need to bring one (pretty small) power brick when I travel. Maybe one of the new Gallium Nitride chargers would take up even less space and weight with the same benefits? If I find one for cheap I'll pick it up.

It's interesting how the small things add up in a pack like this. For example, the larger front zippered pocket of the bag was very full with cables, making loading and unloading the main compartment more difficult than I wanted. I'll need a better way to organize cables in the future, possibly by just putting them in the main compartment.

I brought the CAD U37 for podcasting. The long cable it came with was unweildy, taking up more space in the bag than should have been required. I *used* the mic, but didn't need it. My laptop microphone would have been fine for the video calls I did, and there was a better microphone available at the place I did the podcast.

I got the trial-size Dollar Shave Club products for free a while ago and kept the containers. They've turned out useful for travel, even if they're a little difficult to refill. I made the products last almost exactly 9 nights, shaving cleanly every day. I wish DSC sold their hair products in similarly small containers.

## On my person

The clothing I wore in airports helped lighten the load in my bags:

* My heaviest pair of blue jeans, containing:
  * Trifold wallet
  * Two phones (Sony Xperia X and Moto G5 Plus)
* Active shirt
* Light jacket. In its pockets, I had:
  * Cool-weather gloves
  * Passport wallet
  * Small first-aid items:
    * Ibuprofen
    * Paracetamol (also known as APAP or acetaminophen)
    * Moleskin
    * Bandages
    * Antiseptic towlettes
    * Emergency blanket

The first-aid kit was filled with items that I knew I had a high chance of using while traveling (and the emergency blanket, which was a just-in-case take). This was correct for the Ibuprofen and acetaminophen. I had a chance to use the moleskin or a bandage when a hot spot appeared after many long days of walking. However, a traveller in my group accidentally bought bandages from a poorly-designed vending machine and we used those instead. Either way, these items were fairly small and light. This kit is staying together for future travels.

You'll note the use of two wallets. This is something I learned from some more experienced travelers. If you carry a wallet loaded with expired cards and a bit of cash, a pickpocket or attacker going after you will think they've scored while you retain your truly valuable items.

## Final thoughts

Having two phones on my person in airports is unique to me. I develop [Ubuntu Touch](http://ubuntu-touch.io/), a mobile operating system. I use it every day. However, travel is still a special case for me. The Moto G5 Plus, running stock Android with the Google Apps, had the United app (for boarding passes), Google Maps (which I never used), and Mobile Passport installed. Mobile Passport has become an absolute requirement for re-entering the US. If I had to wait in the immigration line without that app, I would have missed my connecting flight. It's ridiculous that I need to use a proprietary app and service to enter my home country in a reasonable amount of time, but (dripping sarcasm) at least it helps us keep up the illusion of security.

I think this trip went *really* well. Almost every item on this list was used at least once throughout the journey. I only wish I had brought (or had the mind to purchase) some hand lotion. On the flight back my hands ended up cracking quite painfully.

If you have any comments on this post or just want to chat, you can find me on [Twitter @UnivrsalSuprBox](https://twitter.com/univrsalsuprbox), @UniversalSuperBox on Telegram, or @UniversalSuperBox:matrix.org if you're hip and on Matrix.
