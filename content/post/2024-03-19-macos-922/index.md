---
title: "Installing Mac OS 9.22 from USB on a Powermac G4"
date: 2024-03-19T19:50:00-05:00
---

I recently needed to install Mac OS 9 on my old Powermac G4 to get its firmware updated (as part of my last two posts). I didn't want to burn a CD, so I had to do something a bit more interesting.

I retrieved my installer ISO, [Mac OS 9.2.2 Universal (2002 edition) from Macintosh Repository](https://www.macintoshrepository.org/1307-mac-os-9-2-2-universal-2002-edition-). ([archive.org mirror of the same ISO](https://archive.org/details/mac-os-9.2.2-universal-2002-edition))

## First try: Write the ISO to USB

I tried writing my new ISO to a USB stick using GNOME's Disk Image Writer. `dd`, Etcher, or a similar tool would have worked similarly. I plugged that USB stick into the Mac's port labeled USB 1, then booted the device while holding the Option key. Once the menu loaded, I selected the USB drive and hit the Go icon.

I was greeted with a Mac OS 9.2 boot screen, followed by an error message which stopped boot:

> The system software on the startup disk only functions on the original media, not if copied to another drive.
>
> Reboot

Darn. Turns out this Mac OS disk won't boot from writable media. I tried to find some media that I could hardware write-protect to see if that was all the software checks, but I didn't find anything that the Mac could boot from.

## Use the boot kit

[Macintosh Repository's *Mac OS 9.2.2 "boot kit" image*](https://www.macintoshrepository.org/23373-mac-os-9-2-2-boot-kit-for-booting-your-g3-g4-from-an-usb-stick) was the answer. I flashed the `.toast` image to a USB stick using Disk Image Writer and booted it using the Option menu. It booted straight to Mac OS.

Then, I wrote the Mac OS 9 installation ISO to another USB stick. I had to pick one with low power requirements as my USB hub wouldn't report that it was self-powered to Mac OS, so anything that requested 500mA got rejected. An old 2GB Sandisk Cruzer only requested 200mA and was allowed to mount. I plugged the installation USB into the computer and it was recognized. I ran the "Install Mac OS" software successfully.

## Other...

If you don't have two compatible USB drives, you can write the [*MacOS9Lives modified Mac OS 9 ISO* from Macintosh Repository](https://www.macintoshrepository.org/126-mac-os-9-2-2-universal-installer-2013-macos9lives-edition-w-cpu-5-9-rom-10-2-1-for-unsupported-g4-like-fw800-mdd-emac-) to a USB stick and install from it. You can either use that new Mac OS install or rewrite your USB stick to another USB stick, then plug it in and reformat the system using "Install Mac OS".

I'll note that whenever I install from the "Stock" 9.2.2 media, I end up with a system that hangs whenever I attach a USB drive. No idea what that's about.