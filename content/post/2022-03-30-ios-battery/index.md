---
title: "Getting battery health information of attached iOS device from macOS"
date: 2022-03-30T12:12:31-05:00
---

Quick one, mostly for my own memory. I've wanted to find the battery health of attached iOS and iPadOS devices from a Mac, and today I figured out how.

Install `libimobiledevice` first, probably with `brew install libimobiledevice`.

Then, run `idevicediagnostics ioregentry AppleSmartBattery` with a device attached.

Apparently the iPhone 7 and below will use `idevicediagnostics ioregentry AppleARMPMUCharger` instead.

The data comes back as a plist by default. The health information you're looking for is likely `AppleRawMaxCapacity` for the full charge capacity and `DesignCapacity` for the original capacity. Divide `AppleRawMaxCapacity` by `DesignCapacity` and you have the device's battery health.

This might help as people try to figure out how the iOS Battery Health meter determines the health of the battery. The calculation is obviously not as simple as `AppleRawMaxCapacity/DesignCapacity` since it will list 100% health at far below that number in raw mAh. Maybe Apple wrote the algorithm to take battery cycles into account, for example if the raw capacity is 80% and there have been 500 battery cycles the health is 100%?

Not sure, but now we know how to get the info.

Thanks to https://github.com/libimobiledevice/libimobiledevice/issues/1095 for finally unearthing this information after weeks of searching.