---
title: "Crash Course in Ancient Open Firmware"
date: 2024-03-19T19:55:00-05:00
---

This post started as a tangent to [Running Ubuntu 4.10 from USB on a Year 2000 Power Macintosh G4](/2024-02-24-ancient-ubuntu) but spiraled way out of control. I have separated it into this post to hopefully help future travelers in the ways of Open Firmware, at least as it was on old PowerPC Apple computers.

I learned this information through trial and error on an Apple Power Macintosh G4. Some of it might be factually incorrect (but close enough for functionality).

-----

Open Firmware's command line behaves a bit like a Linux shell that you might be used to, but the commands work differently to serve its needs. Remember that Open Firmware's shell is supposed to be just enough to get you debugging the firmware itself or booted into an operating system.

On a PowerPC Mac with Open Firmware, you can boot to the shell prompt by holding the key combination Command+Option+O+F while the system boots. You can also hold down the Option key to enter the graphical Boot Picker interface, then press Control+Z to drop to the shell. Either option has a unique color scheme!

When Open Firmware is waiting for your request, it displays the prompt `0 >`. Sometimes it's possible to get to a non-zero prompt like `1 >`. This seems to be a different context than the usual prompt, commands stop working here. I don't know what that means and I'm not researching it for this tangent. If you get there, usually hitting Enter a few more times will get you back to the `0 >` prompt.

You can run a command by typing your request at the prompt and pressing Enter. Navigation of your typed command is possible by pressing the left and right arrow keys once per character (no auto-repeat here). You can move to the beginning or end of your current line by pressing Control+A or Control+E, respectively. You can navigate limited command history by pressing the up and down arrow keys. Sometimes the history behaves oddly for reasons I haven't been able to figure out, it seems to forget some commands.

Before returning you to the prompt after a command, Open Firmware usually prints a few space characters followed by `ok`.

The shell has a device tree and current device. These work a bit like a Linux root filesystem and the current working directory, respectively. At the root of the tree are subtrees like `pci@<memory address>` and `nvram`. Each driver supported by Open Firmware provides its own nodes under each tree. For example, the USB bus nodes have child nodes like `keyboard` and `mouse`.

Most device nodes that you'll care about have aliases at the root of the device tree. To find these nodes, run the `devalias` command.

You can change where you are on the device tree using the `dev` command. You can list the child nodes under your currently selected device using the `ls` command.

For example, consider the following device layout. This is what you get if you plug an Apple keyboard with an internal USB hub into USB Port 2 on the Power Mac G4, then plug an Apple mouse into the right USB port on the keyboard.

```
/usb0
    /hub@1
        /device@1
            /keyboard@0
            /interface@1
        /mouse@2
```

You can view this node and its children like this:

```
0 > dev usb0  ok
0 > ls
ff8e4840:  /hub@1
ff8e49d0:    /device@1
ff8e4ad0:      /keyboard@0
ff8e4de0:      /interface@1
ff8e4f00:    /mouse@2
 ok
0 >
```

You can specify any node via its full name (like `hub@1`) or you can shorten it to either just the name if it's unique (`hub`) or just the ID (`@1`). So, to select the mouse device, you could use `dev usb0/hub@1/mouse@2` or `dev usb0/hub/mouse` or `dev usb0/@1/@2`.

You can list files on supported filesystems with the `dir` command, but it's a little obtuse. You must specify the device node, partition ID, and the filesystem path. If anything in the command is wrong, you'll get a cryptic error like `method <dir> not found` (you didn't specify a valid device or the filesystem path is invalid), `MAC-PARTS: specified partition is not valid can't OPEN the dir device` (the partition ID you specified probably isn't correct), or `can't OPEN the DIR device` (you specified a valid device but the filesystem path couldn't be found, or you didn't specify a partition at all).

For example, you can list the root directory of a block device attached to the USB 1 port on the back of the Power Mac G4:

```
0 > dir usb1/disk@1:2,\
        2/25/24  2:19: 5 hfsplusdir
     0  2/25/24  2:19: 3 hfsplusfile
```

The following things should be noted about this command:

* The `:2` after the device node specifier tells the command which partition on the device to target. The first partition on an Apple Partition Map device is always 2. An ISO9660 filesystem always shows up as partition 2 as well.
* The comma after the device node and partition specifier tells OpenFirmware where the device path ends and the filesystem path begins.
* You must specify a filesystem path to list, otherwise you will receive the error "`method <dir> not found`".
* HFS+ and ISO9660 path components are separated with backslashes (`\`), not forward slashes.
* Many instructions online include a path starting with two backslashes (`\\`), like `\\yaboot`. Two backslashes is a shortcut to the "blessed" HFS+ path. I'm not entirely sure how that works, but suffice to say on an Ubuntu ISO filesystem, `\\` is a shortcut for `\install\`.

As far as I can tell, my Mac's OpenFirmware can only read Apple Partition Map or ISO9660 (optionally with HFS+ extensions) block devices[1]. It can only list the contents of HFS+ or ISO9660 (again, optionally with HFS+ extensions) partitions. FAT16, FAT32, ext2, and HFS partitions all return errors like, "`MAC-PARTS: specified partition is not valid can't OPEN the DIR device`"[1]. It doesn't seem like there's any reasonable size limit, I got it to list HFS+ partitions of up to 238GiB. Maybe there's another incantation to read MBR (`msdos`) partition tables, but I wasn't sticking around to find it.

If you want to load and execute a file, type the `boot` command followed by the path to the file, like `boot usb1/disk@1:2,\install\yaboot`.

I hope this post is enough to get you up and running with Open Firmware, or if not, helped you figure out how to interpret what it's saying back. I found a few more resources around the web while trying to get my own Mac working, I'll share those next.

## Other resources

* [*Installing Linux on PowerPC Macs* on Low End Mac](https://lowendmac.com/2018/installing-linux-on-powerpc-macs/) ([Internet Archive](https://web.archive.org/web/20230923221458/https://lowendmac.com/2018/installing-linux-on-powerpc-macs/), [archive.is](https://archive.is/G1UgP))
* [*The Open Firmware Wiki* on the MacRumors Forum](https://forums.macrumors.com/threads/the-open-firmware-wiki.2225024/) ([Internet Archive](https://web.archive.org/web/20231227151059/https://forums.macrumors.com/threads/the-open-firmware-wiki.2225024/), [archive.is](https://archive.is/HDeTZ))
* [*Open Firmware Basics and Commands* on Mac Classic](https://mac-classic.com/articles/open-firmware-basics/) ([Internet Archive](https://web.archive.org/web/20240106024854/https://mac-classic.com/articles/open-firmware-basics/), [archive.is](https://archive.is/xtAoj))
* [*Open Firmware Scripts* on Mac Classic](https://mac-classic.com/articles/open-firmware-scripts/) ([Internet Archive](https://web.archive.org/web/20240106012728/https://mac-classic.com/articles/open-firmware-scripts/), [archive.is](https://archive.is/j3s2V))
* [*Open Firmware* on the MorphOS Library](https://library.morph.zone/Open_Firmware) ([Internet Archive](https://web.archive.org/web/20220411023214/https://library.morph.zone/Open_Firmware), [archive.is](https://archive.is/mSI0E))

If you want to get information about the memory in your system, type `dev /memory@() .properties`. This command uses two syntaxes that I haven't explored, the parentheses and whatever you call `.properties`. Seems fun. I don't need to go further down this rabbit hole. Thanks [*Open Firmware - RAM Size* on Super User](https://superuser.com/questions/397592/open-firmware-ram-size) ([Internet Archive](https://web.archive.org/web/20140721122237/https://superuser.com/questions/397592/open-firmware-ram-size), [archive.is](https://archive.is/5rafE)).


---

[1] I upgraded the Mac from its firmware version `2.6f1` to `4.2.8f1` after writing this post. It now seems partially capable of reading MBR partition tables. Partitions are enumerated in the order they are created, starting at `:1`. I can only get it to list the contents of FAT16 filesystems though. It doesn't throw an error for other filesystems, it just doesn't show any contents for the path `\`. Tested with FAT32, HFS+, and ext2. Reading a filesystem on a gpt table just crashes openfirmware. On a mac table, every filesystem except HFS and HFS+ throws `MAC-PARTS: specified partition is not valid can't OPEN the DIR device`. HFS seems to work though I didn't test in-depth, you probably want to use HFS+ anyway.
